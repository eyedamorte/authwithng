import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit{

  registrationForm: FormGroup;

  submitForm(){
    console.log(this.registrationForm.value.login);
  }

  ngOnInit(){
    this.registrationForm = new FormGroup({
      login: new FormControl(),
      password: new FormControl({}),
    })
  }
}
