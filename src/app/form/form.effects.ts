import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { EMPTY } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { AuthService } from './form.service';

@Injectable()
export class FormEffects{

  authSuccess$ = createEffect(() => this.actions$.pipe(
      ofType(authActionsType.AUTH_START),
      mergeMap(() => this.authService.getToken()
        .pipe(
          map(movies => ({ type: authActionsType.AUTH_SUCCESS, payload: token })),
          catchError(() => EMPTY)
        ))
      )
    );

    constructor(
        private actions$: Actions,
        private authService: AuthService
      ) {}
}
