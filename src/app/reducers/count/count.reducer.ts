import { countActionsType, CountActions } from "./count.actions";

export const countNode = 'count';

export interface CountState{
  count: number;
  updatedAt: number;
  title: string;
}
const initialState: CountState = {
  count: 0,
  updatedAt: Date.now(),
  title: null,
};

export const countReducer = (state = initialState, action: CountActions) => {
  switch (action.type) {

    case countActionsType.increase:
      return {
        ...state,
        count: state.count + 1,
        title: action.title,
      };
    case countActionsType.decrease:
      return {
        ...state,
        count: state.count - 1
      };
    case countActionsType.clear:
      return {
        ...state,
        count: 0
      }


    default:
      return state;
  }
  return state;
};
