export enum authActionsType{
  AUTH_START = '[AUTH] start',
  AUTH_SUCCESS = '[AUTH] success',
  AUTH_LOGOUT = '[AUTH] logout'
}


export class AuthLoginAction implements Action {
  readonly type = authActionsType.AUTH_START;
  constructor(public payload: {
    login: string;
    password: string;
  }){}
}

export class AuthSuccessAction implements Action {
  readonly type = authActionsType.AUTH_SUCCESS;
}

export class AuthLogoutAction implements Action {
  readonly type = authActionsType.AUTH_LOGOUT;
}
